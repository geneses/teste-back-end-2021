<?php

use App\Http\Controllers\Api\ConsultasController;
use App\Http\Controllers\Api\HistoricoController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/login', [UserController::class, 'login']);
        Route::post('/logout', [UserController::class, 'logout'])->middleware('apiJWT');
    });

    Route::group(['prefix' => 'user', 'middleware' => 'apiJWT'], function () {
        Route::post('/create', [UserController::class, 'store']);
        Route::get('/historico', [HistoricoController::class, 'index']);
    });

    Route::group(['prefix' => 'cotacoes', 'middleware' => 'apiJWT'], function () {
        Route::get('/moedas', [ConsultasController::class, 'options']);
        Route::get('/ocorrencias/{moedas}', [ConsultasController::class, 'ocorrencias']);
        Route::get('/fechamento', [ConsultasController::class, 'getFechamentoDias']);
        Route::get('/sequencia', [ConsultasController::class, 'getSequencia']);
    });
});