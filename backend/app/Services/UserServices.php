<?php

namespace App\Services;

use App\Exceptions\UnauthorizedException;
use App\Repository\UserRepository;
use Auth;
use Hash;
use Illuminate\Database\Eloquent\Model;

class UserServices
{
    public function __construct(
        public UserRepository $userRepository
    ) {
    }

    public function createUser(array $data): Model
    {
        $data['password'] = $this->encryptPassword($data['password']);
        return $this->userRepository->create($data);
    }

    public function encryptPassword(string $password): string
    {
        return Hash::make($password);
    }

    public function checkCredentials(array $data): array
    {
        if (!$token = Auth::attempt($data))
            throw new UnauthorizedException();
        return $this->respondWithToken($token);
    }

    public function refresh(): array
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function logout()
    {
        auth()->logout();
    }


    protected function respondWithToken(string $token): array
    {
        return [
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'name' => Auth::user()->name
        ];
    }
}