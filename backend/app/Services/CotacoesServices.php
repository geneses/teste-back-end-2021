<?php

namespace App\Services;

use App\Events\HistoricoEvent;
use Auth;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class CotacoesServices
{

    public function getMoedas(): array
    {
        $response = Http::get(env('API_COTACOES') . '/json/available');
        $opces = [];
        foreach ($response->json() as $key => $value) {
            array_push($opces, [
                'code' => $key,
                'name' => $value
            ]);
        }
        return $opces;
    }

    private function getCode(array $data): string
    {
        return $data['code'] . '/' . $data['codein'];
    }

    private function filterData(array $data): array
    {
        $newData = [];

        foreach ($data as $value) {
            array_push($newData, [
                'baixa' => $value['low'],
                'alta' => $value['high'],
                'date' => date("Y-m-d H:i:s", $value['timestamp'])
            ]);
        }
        return array_reverse($newData);
    }

    private function HandleEvent(string $acao, array $data, array $result)
    {
        event(new HistoricoEvent([
            'consulta' => $acao,
            'dados' => $data,
            'resultado' => $result
        ]));
    }

    public function getOcorrencias(string $moedas): array
    {
        $response = Http::get(env('API_COTACOES') . "/last/{$moedas}");
        $data = $response->json();
        $opces = [];
        foreach ($data as $value) {
            array_push($opces, [
                'code' => $this->getCode($value),
                'baixa' => $value['low'],
                'alta' => $value['high']
            ]);
        }
        $this->HandleEvent('ocorrencias', $data, $opces);
        return $opces;
    }

    public function getFechamento(array $data)
    {
        if ($data['tipoPesquisa'] === 2) {
            foreach ($data['intervalo'] as $key => $value) {
                $data['intervalo'][$key] = str_replace("-", "", $value);
            }
            $response = Http::get(env('API_COTACOES') . "/json/daily/{$data['moeda']}", [
                'start_date' => $data['intervalo'][0],
                'end_date' => $data['intervalo'][1]
            ]);
        } else {
            $response = Http::get(env('API_COTACOES') . "/json/daily/{$data['moeda']}/{$data['dias']}");
        }

        $result = [
            'code' => $data['moeda'],
            'fechamentos' => $this->filterData($response->json())
        ];
        $this->HandleEvent('fechamentos', $data, $result);
        return $result;
    }
    public function getSequencias(array $data)
    {
        if ($data['showInterval'] === "true") {

            foreach ($data['intervalo'] as $key => $value) {
                $data['intervalo'][$key] = str_replace("-", "", $value);
            }
            $response = Http::get(env('API_COTACOES') . "/{$data['moeda']}/{$data['sequencia']}", [
                'start_date' => $data['intervalo'][0],
                'end_date' => $data['intervalo'][1]
            ]);
        } else {
            $response = Http::get(env('API_COTACOES') . "/{$data['moeda']}/{$data['sequencia']}");
        }

        $result = [
            'code' => $data['moeda'],
            'sequencias' => $this->filterData($response->json())
        ];
        $this->HandleEvent('sequencias', $data, $result);
        return $result;
    }
}