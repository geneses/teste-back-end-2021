<?php

namespace App\Repository;

use App\Exceptions\Database;
use App\Models\User;
use Log;

class UserRepository
{
    public function create(array $data): User
    {
        // dd($data);
        try {
            return User::create($data);
        } catch (\Exception $e) {
            Log::critical($e->getMessage());
            throw new Database();
        }
    }

    public function findByEmail(string $email): ?User
    {
        return User::where('email', $email)->first();
    }
}