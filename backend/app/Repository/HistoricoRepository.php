<?php

namespace App\Repository;

use App\Exceptions\Database;
use App\Models\Historico;
use Auth;
use Log;

class HistoricoRepository
{
    public function create(array $data): Historico
    {
        try {
            return Historico::create($data);
        } catch (\Exception $e) {
            Log::critical($e->getMessage());
            throw new Database();
        }
    }
    public function getHistoricoUser(): array
    {
        $data = Auth::user()->historico->map(function ($item) {

            $item->dados = json_decode($item->dados, true);
            $item->resultado = json_decode($item->resultado, true);
            return $item;
        });
        return $data->toArray();
    }
}