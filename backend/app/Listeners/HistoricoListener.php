<?php

namespace App\Listeners;

use App\Repository\HistoricoRepository;
use Auth;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class HistoricoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(
        private HistoricoRepository $historicoRepository
    ) {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->data['dados'] = json_encode($event->data['dados'], JSON_UNESCAPED_UNICODE);
        $event->data['resultado'] = json_encode($event->data['resultado'], JSON_UNESCAPED_UNICODE);
        $event->data['user_id'] = Auth::user()->id;
        $this->historicoRepository->create($event->data);
    }
}