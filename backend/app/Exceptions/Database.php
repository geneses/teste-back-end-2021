<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;

class Database extends Exception
{
    public function render(Request $request)
    {
        return response("Erro ao salvar dados no banco!", 500);
    }
}