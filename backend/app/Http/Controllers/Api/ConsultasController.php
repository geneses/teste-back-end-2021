<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Services\CotacoesServices;

class ConsultasController extends Controller
{
    public function __construct(
        public CotacoesServices $cotacoesServices
    ) {
    }

    public function options()
    {
        return $this->cotacoesServices->getMoedas();
    }

    public function ocorrencias($moedas)
    {
        return $this->cotacoesServices->getOcorrencias($moedas);
    }
    public function getFechamentoDias(Request $request)
    {
        return $this->cotacoesServices->getFechamento($request->all());
    }
    public function getSequencia(Request $request)
    {
        return $this->cotacoesServices->getSequencias($request->all());
    }
}