<?php

namespace App\Http\Controllers\Api;

use App\Repository\HistoricoRepository;
use Illuminate\Http\Request;

class HistoricoController extends Controller
{

    public function __construct(
        private HistoricoRepository $historicoRepository
    ) {
    }

    public function index()
    {
        return $this->historicoRepository->getHistoricoUser();
    }
}