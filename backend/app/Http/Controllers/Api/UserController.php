<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\UserRequest;
use App\Services\UserServices;

class UserController extends Controller
{
    public function __construct(
        public UserServices $userServices
    ) {
    }
    public function login(LoginRequest $request)
    {
        return $this->userServices->checkCredentials($request->all());
    }

    public function refresh()
    {
        return $this->userServices->refresh();
    }

    public function logout()
    {
        $this->userServices->logout();
    }

    public function store(UserRequest $request)
    {
        return $this->userServices->createUser($request->all());
    }
}