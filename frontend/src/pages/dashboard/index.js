import Navbar from "../../components/Navbar";
import Graficos from "../../components/graficos";
import Usuario from "../../components/usuario/index.vue";
import Historico from "../../components/historico/index.vue";

export default {
  components: {
    Graficos,
    Navbar,
    Usuario,
    Historico,
  },
  mounted() {
    document.querySelector("body").style.background = "transparent";
  },
};
