export default function(element, data, labels, baixa, alta) {
  data.map((item) => {
    baixa.push(item.baixa);
    alta.push(item.alta);
    labels.push(item.label);
  });
  if (element !== null) {
    let { datasets } = element.data;
    datasets = datasets.map((item) => {
      if (item.label === "Baixa") {
        return {
          ...item,
          data: baixa,
        };
      }
      return {
        ...item,
        data: alta,
      };
    });
    element.data.datasets = datasets;
    element.data.labels = labels;
    element.update();
  }
}
