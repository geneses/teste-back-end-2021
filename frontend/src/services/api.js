import Axios from "axios";
// import Vue from 'vue'
// "https://economia.awesomeapi.com.br",
const baseUrl = "http://localhost:8000/v1";
const api = Axios.create({
  baseURL: baseUrl,
});
api.interceptors.request.use((config) => {
  if (config.url !== "/auth/login") {
    let { access_token } = JSON.parse(localStorage.getItem("token"));
    config.headers["Authorization"] = `Bearer ${access_token}`;
  }
  return config;
});

api.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      alert(error.response.data.status);
      localStorage.removeItem("token");
      window.location = "/";
    }
  }
);
export default api;
