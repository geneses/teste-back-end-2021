import Vue from "vue";
import Vuex from "vuex";

import fechamento from "./modules/fechamento";
import ocorrencias from "./modules/ocorrencias";
import sequencias from "./modules/sequencias";
import utils from "./modules/utils";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    fechamento,
    ocorrencias,
    sequencias,
    utils,
  },
});
