import actions from "./actions";
export default {
  state: {
    tabActive: 0,
    historico: [],
    loading: {
      login: false,
      usuario: false,
    },
    error: {
      login: [],
    },
  },
  mutations: {
    SET_TAB(state, data = 0) {
      state.tabActive = data;
    },
    SET_HISTORICO(state, data) {
      state.historico = data;
    },
    SET_LOADING(state, data) {
      state.loading = { ...state.loading, ...data };
    },
    SET_ERROR(state, error) {
      state.error = { ...state.error, ...error };
    },
  },
  actions,
};
