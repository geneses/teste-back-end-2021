import api from "../../../services/api";
import moment from "moment";
export default {
  async login(contex, form) {
    try {
      contex.commit("SET_LOADING", { login: true });
      form = { ...form, password: form.senha };
      delete form["senha"];
      let { data } = await api.post("/auth/login", form);
      localStorage.setItem("token", JSON.stringify(data));
    } catch (error) {
      let { response } = error;
      // console.log(response);
      if (response !== undefined) {
        contex.commit("SET_ERROR", {
          login: [response.data],
        });
      }
      console.error(error);
    } finally {
      contex.commit("SET_LOADING", { login: false });
    }
  },
  async logout(contex) {
    try {
      contex.commit("SET_LOADING", { login: true });
      await api.post("/auth/logout");
      localStorage.removeItem("token");
    } catch (error) {
      console.log(error);
      contex.commit("SET_ERROR", {
        login: ["error No logout"],
      });
    } finally {
      contex.commit("SET_LOADING", { login: false });
    }
  },
  async cadastrarUsuario(context, data) {
    try {
      data = {
        email: data.email,
        password: data.senha,
        name: data.nome,
      };
      context.commit("SET_LOADING", {
        usuario: true,
      });
      await api.post("/user/create", data);
    } catch (error) {
      alert("erro ao cadastrar");
    } finally {
      context.commit("SET_LOADING", {
        usuario: false,
      });
    }
  },
  async getHistorico(context) {
    try {
      let { data } = await api.get("/user/historico");
      data = data.map((item) => {
        return {
          consulta: item.consulta,
          "data e hora": moment(item.created_at).format("DD/MM/YYYY"),
        };
      });

      context.commit("SET_HISTORICO", data);
    } catch (error) {
      console.error(error);
    }
  },
};
