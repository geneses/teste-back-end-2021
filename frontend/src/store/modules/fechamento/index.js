import mutations from "./mutations";
import actions from "./actions";
export default {
  state: {
    data: {
      fechamentos: [],
    },
    loading: {
      getData: false,
    },
  },
  mutations,
  actions,
};
