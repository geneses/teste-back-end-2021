// import moment from "moment";
import api from "../../../services/api";
export default {
  async getFechamentoDias(context, params) {
    try {
      context.commit("LOADING_FECHAMENTOS", {
        getData: true,
      });
      let { data } = await api.get(`/cotacoes/fechamento/`, {
        params: {
          ...params,
        },
      });
      context.commit("DATA_FECHAMENTOS", {
        fechamentos: data,
      });
    } catch (error) {
      console.error(error);
    } finally {
      context.commit("LOADING_FECHAMENTOS", {
        getData: false,
      });
    }
  },
};
