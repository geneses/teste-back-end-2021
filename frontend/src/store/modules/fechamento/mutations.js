export default {
  DATA_FECHAMENTOS(state, data) {
    state.data = { ...state.data, ...data };
  },
  LOADING_FECHAMENTOS(state, data) {
    state.loading = { ...state.loading, ...data };
  },
};
