import api from "../../../services/api";
export default {
  async getOptions(context) {
    try {
      context.commit("LOADING_OCORRENCIAS", {
        getOptions: true,
      });
      let { data } = await api.get("cotacoes/moedas");
      context.commit("DATA_OCORRENCIAS", {
        options: data,
      });
    } catch (error) {
      console.error(error);
    } finally {
      context.commit("LOADING_OCORRENCIAS", {
        getOptions: false,
      });
    }
  },
  async getOcorrencias(context, optionsData) {
    try {
      if (!optionsData.length) {
        throw new Error("Não contem nenhuma opção");
      }
      context.commit("LOADING_OCORRENCIAS", {
        getData: true,
      });
      optionsData = optionsData.join(",");
      let { data } = await api.get(`/cotacoes/ocorrencias/${optionsData}`);

      context.commit("DATA_OCORRENCIAS", {
        ocorrencias: data,
      });
    } catch (error) {
      console.error(error);
    } finally {
      context.commit("LOADING_OCORRENCIAS", {
        getData: false,
      });
    }
  },
};
