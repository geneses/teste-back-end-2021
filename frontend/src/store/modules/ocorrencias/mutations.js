export default {
  DATA_OCORRENCIAS(state, data) {
    state.data = { ...state.data, ...data };
  },
  LOADING_OCORRENCIAS(state, data) {
    state.loading = { ...state.loading, ...data };
  },
};
