import mutations from "./mutations";
import actions from "./actions";

export default {
  state: {
    data: {
      ocorrencias: [],
      options: [],
    },
    loading: {
      getOptions: false,
      getData: false,
    },
  },
  mutations,
  actions,
};
