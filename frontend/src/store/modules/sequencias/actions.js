import api from "../../../services/api";
export default {
  async getSequencias(context, params) {
    try {
      context.commit("LOADING_SEQUENCIA", {
        getData: true,
      });
      let { data } = await api.get(`/cotacoes/sequencia/`, {
        params: {
          ...params,
        },
      });

      context.commit("DATA_SEQUENCIA", data);
    } catch (error) {
      console.error(error);
    } finally {
      context.commit("LOADING_SEQUENCIA", {
        getData: false,
      });
    }
  },
};
