import mutations from "./mutations";
import actions from "./actions";
export default {
  state: {
    data: null,
    loading: {
      getData: false,
    },
  },
  mutations,
  actions,
};
