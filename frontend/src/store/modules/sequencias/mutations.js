export default {
  DATA_SEQUENCIA(state, data) {
    state.data = data;
  },
  LOADING_SEQUENCIA(state, data) {
    state.loading = { ...state.loading, ...data };
  },
};
