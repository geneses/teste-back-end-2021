import $ from "jquery";
import Chart from "chart.js/auto";
import { mapState } from "vuex";
import moment from "moment";
import dataFilterGraph from "../../../utils";

const optionSelected = ["USD-BRL"];
let dataBaixa = [];
let dataAlta = [];
let labelsData = [];

export default {
  data() {
    return {
      elementData: null,
      elementGraph: null,
      elementInterval: null,
      showInterval: false,
      moeda: optionSelected[0],
      sequencia: 5,
      intervalo: [moment().format("YYYY-MM-DD"), moment().format("YYYY-MM-DD")],
    };
  },
  computed: {
    ...mapState({
      options: (state) => {
        return state.ocorrencias.data.options.map((item) => {
          let dataSelect2 = {
            id: item.code,
            text: item.code,
            name: item.name,
          };
          if (optionSelected.some((optionItem) => optionItem === item.code)) {
            return {
              ...dataSelect2,
              selected: true,
            };
          }
          return dataSelect2;
        });
      },
      tabActive: (state) => state.utils.tabActive,
      sequencias: (state) => {
        let data = state.sequencias.data;
        let dataFilter = [];
        if (data !== null) {
          dataFilter = data.sequencias.map((item) => {
            return {
              ...item,
              label: moment(item.date).format("DD/MM/YYYY"),
            };
          });
        }
        return {
          ...data,
          sequencias: dataFilter,
        };
      },
      loading: (state) => state.sequencias.loading.getData,
    }),
  },
  watch: {
    tabActive(value) {
      if (value === 2) {
        this.setupComponent();
      } else {
        this.destroyComponent();
      }
    },
  },
  methods: {
    async getData(args) {
      await this.$store.dispatch("getSequencias", args);
      dataBaixa = [];
      dataAlta = [];
      labelsData = [];
      dataFilterGraph(
        this.elementGraph,
        this.sequencias.sequencias,
        labelsData,
        dataBaixa,
        dataAlta
      );
    },
    handleSearch() {
      if (this.moeda === null) {
        this.$bvToast.toast("Moeda Obrigatório", {
          title: "Erro no campo abaixo",
          variant: "danger",
          solid: true,
        });
      } else if (this.sequencia === null || this.sequencia === "") {
        this.$bvToast.toast("Sequência Obrigatório", {
          title: "Erro no campo abaixo",
          variant: "danger",
          solid: true,
        });
      } else {
        this.getData({
          moeda: this.moeda,
          showInterval: this.showInterval,
          sequencia: this.sequencia,
          intervalo: this.intervalo,
        });
      }
    },
    createDataRangePicker() {
      this.elementInterval = this.$refs.sequenciaIntervalo;
      $(this.elementInterval).daterangepicker({
        showDropdowns: true,
        drops: "up",
        startDate: moment(),
        maxDate: moment(),
        locale: {
          format: "DD/MM/YYYY",
        },
      });
    },
    eventsDataRangepicker() {
      let vm = this;
      $(this.elementInterval).on("apply.daterangepicker", function() {
        let datas = $(this)
          .val()
          .split("-");
        if (datas.length) {
          datas = datas.map((item) => {
            return moment(item, "DD/MM/YYYY").format("YYYY-MM-DD");
          });
          vm.intervalo = datas;
        }
      });
    },
    createSelect2() {
      this.elementData = this.$refs.dataSequencia;
      $(this.elementData).select2({
        width: "100%",
        allowClear: true,
        placeholder: "select..",
        data: this.options,
      });
    },
    eventsSelect2() {
      let vm = this;
      $(this.elementData).on("change", async function() {
        vm.moeda = $(this).val();
        $(this).select2("close");
      });
    },
    createGraph() {
      this.elementGraph = new Chart(this.$refs.sequencia, {
        type: "line",
        data: {
          labels: labelsData,
          datasets: [
            {
              label: "Baixa",
              data: dataBaixa,
              borderColor: "rgba(255, 177, 193, 1)",
              backgroundColor: "rgba(255, 177, 193, 0.5)",
              fill: false,
              tension: 0.4,
            },
            {
              label: "Alta",
              data: dataAlta,
              borderColor: "rgba(154, 208, 245, 1)",
              backgroundColor: "rgba(154, 208, 245, 0.5)",
              fill: false,
              tension: 0.4,
            },
          ],
        },
        options: {
          responsive: true,
          interaction: {
            intersect: false,
            mode: "index",
          },
          plugins: {
            legend: {
              position: "top",
            },
            title: {
              display: true,
              text: "Sequências",
            },
          },
        },
      });
    },
    async setupComponent() {
      await this.getData({
        sequencia: this.sequencia,
        moeda: this.moeda,
        intervalo: this.intervalo,
        showInterval: this.showInterval,
      });
      this.createSelect2();
      this.eventsSelect2();
      this.createDataRangePicker();
      this.eventsDataRangepicker();
      this.createGraph();
    },
    destroyComponent() {
      if (this.elementData !== null) {
        $(this.elementData)
          .off()
          .select2("destroy");
        $(this.elementData)
          .find("option")
          .remove();
      }
    },
  },
};
