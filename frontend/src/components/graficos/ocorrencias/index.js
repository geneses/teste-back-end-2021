import Chart from "chart.js/auto";
import $ from "jquery";
import { mapState } from "vuex";
import dataFilterGraph from "../../../utils";

const optionSelected = ["USD-BRL", "EUR-BRL"];
let dataBaixa = [];
let dataAlta = [];
let labelsData = [];
export default {
  data() {
    return {
      elementData: null,
      elementGraph: null,
      value: [],
    };
  },
  computed: {
    ...mapState({
      options: (state) => {
        return state.ocorrencias.data.options.map((item) => {
          let dataSelect2 = {
            id: item.code,
            text: item.code,
            name: item.name,
          };
          if (optionSelected.some((optionItem) => optionItem === item.code)) {
            return {
              ...dataSelect2,
              selected: true,
            };
          }
          return dataSelect2;
        });
      },
      dataOcorrencias: (state) => {
        return state.ocorrencias.data.ocorrencias.map((item) => {
          return {
            ...item,
            label: item.code,
          };
        });
      },
    }),
  },
  methods: {
    createGraph() {
      this.elementGraph = new Chart(this.$refs.ocorrencias, {
        type: "bar",
        data: {
          labels: labelsData,
          datasets: [
            {
              label: "Baixa",
              data: dataBaixa,
              borderColor: "rgba(255, 177, 193, 1)",
              backgroundColor: "rgba(255, 177, 193, 0.5)",
              borderWidth: 1,
            },
            {
              label: "Alta",
              data: dataAlta,
              borderColor: "rgba(154, 208, 245, 1)",
              backgroundColor: "rgba(154, 208, 245, 0.5)",
              borderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: "top",
            },
            title: {
              display: true,
              text: "Ultimas ocorrencias",
            },
            scales: {
              y: {
                beginAtZero: true,
              },
            },
          },
        },
      });
    },
    async getData(args) {
      await this.$store.dispatch("getOcorrencias", args);
      this.value = args;
      dataBaixa = [];
      dataAlta = [];
      labelsData = [];
      dataFilterGraph(
        this.elementGraph,
        this.dataOcorrencias,
        labelsData,
        dataBaixa,
        dataAlta
      );
    },
    async getOptions() {
      await this.$store.dispatch("getOptions");
    },
    createSelect2() {
      this.elementData = this.$refs.dataOcorrencias;
      $(this.elementData).select2({
        width: "100%",
        allowClear: true,
        placeholder: "select..",
        data: this.options,
      });
    },
    eventsSelect2() {
      let vm = this;
      $(this.elementData).on("change", async function() {
        let moedas = $(this).val();
        if (moedas.length) {
          await vm.getData(moedas);
        } else {
          vm.elementGraph.clear();
        }
        $(this).select2("close");
      });
    },
  },
  async mounted() {
    await this.getOptions();
    await this.getData(optionSelected);
    this.createGraph();
    this.createSelect2();
    this.eventsSelect2();
  },
  breforeDestroy() {
    $(this.elementData)
      .off()
      .select2("destroy");
  },
};
