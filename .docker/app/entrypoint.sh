#!/bin/bash

## Front-end
cd frontend
yarn install

## backend
cd ../backend
cp .env.example .env
composer install

php-fpm